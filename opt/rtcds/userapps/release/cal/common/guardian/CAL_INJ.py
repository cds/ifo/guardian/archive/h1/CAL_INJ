# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-

"""
base guardian module for hardware injections

This module defines the behavior for all transient injections guardian states.

The pathway for a successful injection is:
  (1) The guardian node will begin in the DISABLED state, ie. do
      not read the schedule or perform any injections.
  (2) Request the WAIT_FOR_NEXT_INJECT state, this will continously check
      if there is an injection in the near future.
  (3) If there is an injection in the near future, then jump
      to the CHECK_SCHEDULE_TIMES state to do a sanity check that two
      injections are not too close. Only do this once after the node has
      been reloaded.
  (4) Then slide to the CREATE_GRACEDB_EVENT state to upload a
      HardwareInjection event to GraceDB.
  (5) Then create an awg stream instance in CREATE_AWG_STEAM.
  (6) Then read the waveform file, open the awg stream, and send the time
      series data to the stream in the READ_WAVEFORM state.
  (7) Now wait in AWG_STREAM_OPEN_PREINJECT until it is time to do the
      injection. We wait here because there is some latency to move from
      WAIT_FOR_NEXT_INJECT to _INJECT_STATE_ACTIVE.
  (8) Several seconds before it is time to perform the injection, jump to the
      appropreiate _INJECT_STATE_ACTIVE subclass. Close the stream, this will
      perform the hardware injection.
  (9) Jump to the INJECT_SUCCESS state, make sure stream is closed, then jump
      back to the WAIT_FOR_NEXT_INJECT state.

Other states are for failures or waiting for external alerts.

Other operating procedures:
  * Schedule validation should be done after updating the schedule each time,
    the script is committed to: ../scripts/guardian_inj_validate_schedule.py
  * To update the schedule, you need to reload the guardian node.
  * To cancel an injection request KILL_INJECT.

2016 - Christopher M. Biwer
"""

import injtools
import os.path
import sys
import traceback
from gpstime import gpstime
from guardian import GuardStateDecorator

# name of channel to inject transient signals
model_name = "CAL-PINJX"
exc_channel_name = model_name + "_TRANSIENT_EXC"

# name of channel to write legacy tinj EPIC records
type_channel_name = model_name + "_TINJ_TYPE"
start_channel_name = model_name + "_TINJ_START"
end_channel_name = model_name + "_TINJ_ENDED"
outcome_channel_name = model_name + "_TINJ_OUTCOME"

# name of channel to check for external alerts
exttrig_channel_name = "CAL-INJ_EXTTRIG_ALERT_TIME"

# name of channel to check if detector is locked
lock_channel_name = "GRD-ISC_LOCK_OK"

# name of channel to check if intent mode on
obs_channel_name = "ODC-MASTER_CHANNEL_LATCH"

# bitmask to use to check if intent mode on this is AND'ed
# with obs_channel_name
obs_bitmask = 1

# seconds to wait after an external alert before performing injections again
exttrig_wait_seconds = 3600

# seconds to check for an imminent hardware injection
# eg. if set to 300 seconds then begin uploading to GraceDB and read
# waveform file 300 seconds in advance of hardware injection start time
imminent_seconds = 300

# maximum seconds in advance before jump to injection state
# eg. if set to 2 seconds then jump from AWG_STREAM_OPEN_PREINJECT to
# _INJECT_STATE_ACTIVE 2 seconds in advance of hardware injection start time
jump_to_inj_seconds = 20

# sample rate of excitation channel and waveform files
sample_rate = 16384

# path to schedule file
schedule_path = os.path.dirname(__file__) + "/schedule/schedule_1148558052.txt"

# read schedule
hwinj_list = injtools.read_schedule(schedule_path)

# a boolean that turns off code blocks to run guardian daemon for development
# to test reading schedule: `guardian INJ WAIT_FOR_NEXT_INJECT`
# to test awg: `guardian INJ CREATE_AWG_STREAM INJECT_CBC_ACTIVE`
dev_mode = True

def check_exttrig_alert(hwinj_list, failure_state):
    """ Create a GuardStateDecorator to check if there is an external alert.

    Parameters
    ----------
    hwinj_list: list
        A list of HardwareInjection instances.
    failure_state: str
        Name of the GuardState to jump transition to if there is an external
        alert found.

    Returns
    ----------
    check_exttrig_alert_decorator: GuardStateDecorator
        A GuardStateDecorator that checks for external alerts.
    """

    # FIXME: do not check
    if 1:
        class gracedb_post_inj_message_decorator(GuardStateDecorator):
            pass
        return gracedb_post_inj_message_decorator

    class check_exttrig_alert_decorator(GuardStateDecorator):
        """ Decorator for GuardState that will check for an external alert.
        """

        def pre_exec(self):
            """ Do this before entering the GuardState.
            """

            # check if external alert within exttrig_wait_seconds seconds
            # in the past
            exttrig_alert_time = injtools.check_exttrig_alert(exttrig_channel_name,
                                                     exttrig_wait_seconds)
            if exttrig_alert_time:

                # if there is an external alert then close all streams
                try:
                    injtools.close_all_streams(hwinj_list)
                except:
                    message = traceback.print_exc(file=sys.stdout)
                    log(message)
                    return "FAILURE_TO_KILL_STREAM"

                return failure_state

    return check_exttrig_alert_decorator

def gracedb_post_inj_message(hwinj_list, message, look_back_seconds):
    """ Create a GuardStateDecorator that will upload amessage to the most
    recent GraceDB HardwareInjeciton event.

    Parameters
    ----------
    hwinj_list: list
        A list of HardwareInjection instances.
    message: str
        Text to upload to GraceDB event log.
    look_back_seconds: int
        Number of seconds to look back in time to see if there is a GraceDB
        event.

    Returns
    ----------
    gracedb_post_inj_message_decorator: GuardStateDecorator
        A GuardStateDecorator that will post a message to a GraceDB event log.
    """

    # FIXME: do not upload to GraceDB
    if 1:
        class gracedb_post_inj_message_decorator(GuardStateDecorator):
            pass
        return gracedb_post_inj_message_decorator

    class gracedb_post_inj_message_decorator(GuardStateDecorator):
        """ Decorator for GuardState that will upload a message to GraceDB.
        """

        def pre_exec(self):
            """ Do this before entering the GuardState.
            """

            # get last hardware injection from schedule
            hwinj = injtools.check_last_injection(hwinj_list, imminent_seconds)
            if hwinj:

                # upload success message for GraceDB event
                if hwinj.gracedb_id:
                    try:
                        gracedb_upload_message(hwinj.gracedb_id, message)

                    # if an unexpected error was encountered then
                    # jump to failure state
                    except:
                        message = traceback.print_exc(file=sys.stdout)
                        log(message)
                        return "FAILURE_ADDING_GRACEDB_MESSAGE"

            # if no GraceDB ID found then go to failuire state
            log("Could not find GraceDB ID.")
            return "FAILURE_TO_FIND_GRACEDB_ID"

    return gracedb_post_inj_message_decorator

def kill_all_streams(hwinj_list):
    """ Create a GuardStateDecorator that aborts all streams and resets the
    HardwareInjection.stream class attribute to None.

    Parameters
    ----------
    hwinj_list: list
        A list of HardwareInjection instances.

    Returns
    ----------
    kill_all_streams_decorator: GuardStateDecorator
        A GuardStateDecorator that aborts all streams.
    """

    class kill_all_streams_decorator(GuardStateDecorator):
        """ Decorator for GuardState that will aborts all streams and then
        resets them to None.
        """

        def pre_exec(self):
            """ Do this before entering the GuardState.
            """

            # close all streams
            try:
                injtools.close_all_streams(hwinj_list)
            except:
                message = traceback.print_exc(file=sys.stdout)
                log(message)
                return "FAILURE_TO_KILL_STREAM"

    return kill_all_streams_decorator

class INIT(injtools.HwinjGuardState):
    """ The INIT state is the first state entered when starting the Guardian
    daemon. It will run INIT.main once where there will be a jump transition
    to the DISABLED state.
    """

    # determines if state appears on guardian MEDM screen dropdown menu
    request = False

    def main(self):
        """ Execute method once.
        """

        return "DISABLED"

class DISABLED(injtools.HwinjGuardState):
    """ The DISABLED state is for when hardware injections have been disabled
    manually. The DISABLED state will not be left unless the operator requests.
    """

    # assign index for state
    index = 10

    # determines if state appears on guardian MEDM screen dropdown menu
    request = True

    # automatically assign edges from every other state
    goto = True

    @kill_all_streams(hwinj_list)
    def main(self):
        """ Execute method once.
        """

        # set legacy TINJ_OUTCOME value for injections disabled or paused
        ezca[outcome_channel_name] = -3

        return True

    def run(self):
        """ Execute method in a loop.
        """
        return True

class WAIT_FOR_NEXT_INJECT(injtools.HwinjGuardState):
    """ The WAIT_FOR_NEXT_INJECT state continuously loops checking for external
    alerts and if there is an imminent hardware injection.

    An imminment hardware injection is defined by imminent_seconds in
    seconds. If an imminent hardware injection is found then there will be a
    jump transition to the CREATE_GRACEDB_EVENT state.

    An external alert will cause a jump transition to the EXTTRIG_ALERT_ACTIVE
    state if it is within exttrig_wait_seconds seconds.
    """

    # assign index for state
    index = 20

    # determines if state appears on guardian MEDM screen dropdown menu
    request = True

    @kill_all_streams(hwinj_list)
    def main(self):
        """ Execute method once.
        """
        return True

    @check_exttrig_alert(hwinj_list, "EXTTRIG_ALERT_ACTIVE")
    def run(self):
        """ Execute method in a loop.
        """

        # get hardware injection in the future that is soonest
        self.hwinj = injtools.check_imminent_injection(hwinj_list, imminent_seconds)

        # jump transition to PREP state if imminent hardware injection
        if self.hwinj:

            # in dev mode ignore if detector is locked
            # check if detector is locked
            if ezca[lock_channel_name] == 1 or dev_mode:

                # check if detector in desired observing mode and
                # then make a jump transition to CREATE_GRACEDB_EVENT state
                latch = ezca[obs_channel_name] & obs_bitmask
                if ( latch == 1 and self.hwinj.observation_mode == 1 ) or \
                        ( latch == 0 and self.hwinj.observation_mode == 0 ):

                    # legacy of the old setup to set TINJ_START_TIME
                    current_gps_time = gpstime.tconvert("now").gps()
                    ezca[start_channel_name] = current_gps_time

                    return "CHECK_SCHEDULE_TIMES"

                # set legacy TINJ_OUTCOME value for detector not in desired
                # observation mode
                else:
                    log("Ignoring hardware injection since detector is not in " \
                        + "the desired observation mode.")
                    ezca[outcome_channel_name] = -5

            # set legacy TINJ_OUTCOME value for detector not locked
            else:
                log("Ignoring hardware injection since detector is not locked.")
                ezca[outcome_channel_name] = -6

        return True

class EXTTRIG_ALERT_ACTIVE(injtools.HwinjGuardState):
    """ The EXTTRIG_ALERT_ACTIVE state continuously loops checking
    if the most recent external alert is not within exttrig_wait_seconds
    seconds. Once the external alert is far enough in the past there will be a
    jump transition to the WAIT_FOR_NEXT_INJECT state.
    """

    # assign index for state
    index = 30

    # determines if state appears on guardian MEDM screen dropdown menu
    request = False

    def run(self):
        """ Execute method in a loop.
        """

        # check if not external alert
        exttrig_alert_time = injtools.check_exttrig_alert(exttrig_channel_name,
                                                 exttrig_wait_seconds)
        if not exttrig_alert_time:
            return "WAIT_FOR_NEXT_INJECT"

class CHECK_SCHEDULE_TIMES(injtools.HwinjGuardState):
    """ The CHECK_SCHEDULE_TIMES state checks if two injections are too close
    together. This is a safeguard against a user not validating the schedule
    themselves.
    """

    # assign index for state
    index = 35

    # determines if state appears on guardian MEDM screen dropdown menu
    request = False

    def main(self):
        """ Execute this method once.
        """

        # get hardware injection in the future that is soonest
        self.hwinj = injtools.check_imminent_injection(hwinj_list, imminent_seconds)
        if not self.hwinj: return "FAILURE_INJECT_IN_PAST"

        # get a sorted list of hardware injections by their start time
        sorted_hwinj_list = sorted(hwinj_list, key=lambda hwinj: hwinj.schedule_time)

        # if this is the first injection that will be performed then do check
        # otherwise continue because check would have already been done
        if self.hwinj.schedule_time == sorted_hwinj_list[0].schedule_time \
                and len(sorted_hwinj_list) > 1:

            # check that no two injections are too close together
            # this is a safeguard to do before an injection in case someone
            # did not validate the schedule
            for hwinj_1, hwinj_2 in zip(sorted_hwinj_list, sorted_hwinj_list[1:]):
                dt = hwinj_2.schedule_time - hwinj_1.schedule_time
                if dt > 0 and abs(dt) < imminent_seconds:
                    message = "Schedule has two injections %f seconds"%dt \
                        + " apart but must be at least %f"%imminent_seconds \
                        + " seconds apart"
                    log(message)
                    log("Injections are %s and %s"%(str(hwinj_1), str(hwinj_2)))
                    return "FAILURE_SCHEDULED_TWO_INJECT_TOO_CLOSE"

        # print statement
        else:
            log("Skipping checking schedule times since its already been done.")

        return True

class CREATE_GRACEDB_EVENT(injtools.HwinjGuardState):
    """ The CREATE_GRACEDB_EVENT state uploads a HardwareInjection entry
    to GraceDB.
    """

    # assign index for state
    index = 40

    # determines if state appears on guardian MEDM screen dropdown menu
    request = False

    @check_exttrig_alert(hwinj_list, "ABORT_INJECT_FOR_EXTTRIG")
    def main(self):
        """ Execute method once.
        """

        # FIXME: skip gracedb upload because packages are missing
        if 1:
            return True

        # get hardware injection in the future that is soonest
        self.hwinj = injtools.check_imminent_injection(hwinj_list, imminent_seconds)
        if not self.hwinj: return "FAILURE_INJECT_IN_PAST"

        # try to upload an event to GraceDB
        try:

            # map injection states to GraceDB groups
            gracedb_group_dict = {
                "INJECT_CBC_ACTIVE" : "CBC",
                "INJECT_BURST_ACTIVE" : "Burst",
                "INJECT_STOCHASTIC_ACTIVE" : "Stochastic",
                "INJECT_DETCHAR_ACTIVE" : "Burst",
                "TEST" : "Test",
            }
            group = gracedb_group_dict[self.hwinj.schedule_state]

            # upload hardware injection to GraceDB
            self.hwinj.gracedb_id = injtools.gracedb_upload_injection(self.hwinj,
                                           [ezca.ifo], group=group)
            log("GraceDB ID is " + self.hwinj.gracedb_id)

        # if an unexpected error was encountered then jump to failure state
        except:
            message = traceback.print_exc(file=sys.stdout)
            log(message)
            return "FAILURE_CREATE_GRACEDB_EVENT"

        return True

class CREATE_AWG_STREAM(injtools.HwinjGuardState):
    """ The CREATE_AWG_STREAM state will create a awg.ArbitraryStream instance
    for the most imminent hardware injection. The stream can be called by
    the class attribute HardwareInjection.stream.
    """

    # assign index for state
    index = 50

    # determines if state appears on guardian MEDM screen dropdown menu
    request = False

    @check_exttrig_alert(hwinj_list, "ABORT_INJECT_FOR_EXTTRIG")
    def main(self):
        """ Execute method once.
        """

        # get hardware injection in the future that is soonest
        self.hwinj = injtools.check_imminent_injection(hwinj_list, imminent_seconds)
        if not self.hwinj: return "FAILURE_INJECT_IN_PAST"

        # create an ArbitraryStream for HardwareInjection
        # this is the object from the awg module that will control
        # the injection
        try:
            self.hwinj.create_stream(ezca.ifo + ":" + exc_channel_name, sample_rate)
        except:
            message = traceback.print_exc(file=sys.stdout)
            log(message)
            return "FAILURE_TO_CREATE_STREAM"

        return True

class READ_WAVEFORM(injtools.HwinjGuardState):
    """ The READ_WAVEFORM state reads the data from the waveform file and
    sends it to the HardwareInjection stream. The stream is opened right
    before sending the waveform.
    """

    # assign index for state
    index = 60

    # determines if state appears on guardian MEDM screen dropdown menu
    request = False

    @check_exttrig_alert(hwinj_list, "ABORT_INJECT_FOR_EXTTRIG")
    def main(self):
        """ Execute method once.
        """

        # get hardware injection in the future that is soonest
        self.hwinj = injtools.check_imminent_injection(hwinj_list, imminent_seconds)
        if not self.hwinj: return "FAILURE_INJECT_IN_PAST"

        # legacy of the old setup to set TINJ_TYPE
        tinj_type_dict = {
            "INJECT_CBC_ACTIVE" : 1,
            "INJECT_BURST_ACTIVE" : 2,
            "INJECT_DETCHAR_ACTIVE" : 3,
            "INJECT_STOCHASTIC_ACTIVE" : 4,
        }
        ezca[type_channel_name] = tinj_type_dict[self.hwinj.schedule_state]

        # create a dict for formatting strings
        format_dict = {
            "ifo" : ezca.ifo,
        }

        # try to read waveform file
        try:
            log("Sending waveform data for injection at %d"%self.hwinj.schedule_time)
            data = self.hwinj.read_data(format_dict=format_dict)

            # open awg stream
            self.hwinj.stream.open()

            # send excitation data to front end
            self.hwinj.stream.append(data, scale=self.hwinj.scale_factor)

        # if an unexpected error was encountered then jump to failure state
        except:
            message = traceback.print_exc(file=sys.stdout)
            log(message)
            return "FAILURE_READ_WAVEFORM"

        return True

class AWG_STREAM_OPEN_PREINJECT(injtools.HwinjGuardState):
    """ We open the stream with awg in advance of the start time of the
    injection. Therefore there is some time before the injection will begin.
    The AWG_STREAM_OPEN continuously checks the time, and for external alerts.
    Then once its time for the injection to begin, it will transition to the
    corresponding _INJECT_STATE_ACTIVE subclass for the hardware injection.

    Having this state is a safe-guard against long upload times to GraceDB or
    reading large waveform files. If there is a FAILURE_INJECT_IN_PAST leading
    up to this state, you should try increasing the imminent_seconds variable.
    """

    # assign index for state
    index = 70

    # determines if state appears on guardian MEDM screen dropdown menu
    request = False

    def main(self):
        """ Execute method once.
        """
 
        # get hardware injection in the future that is soonest
        self.hwinj = injtools.check_imminent_injection(hwinj_list, imminent_seconds)
        if not self.hwinj: return "FAILURE_INJECT_IN_PAST"

        # handle run explicitly in main because we would like the requested
        # state to be WAIT_FOR_NEXT_INJECT->INJECT_SUCCESS and therefore
        # there is are a series of edges that connect those two states
        # so if you remove this loop without this loop guardian will
        # immediately jump to _INJECT_STATE_ACTIVE much longer before
        # the injection
        while True:
            out = self.run()
            if out: return out

        return True

    @check_exttrig_alert(hwinj_list, "ABORT_INJECT_FOR_EXTTRIG")
    def run(self):
        """ Execute method in a loop.
        """

        # check if its time to jump to the corresponding _INJECT_STATE_ACTIVE subclass
        current_gps_time = gpstime.tconvert("now").gps()
        if current_gps_time > self.hwinj.schedule_time - jump_to_inj_seconds:
            return self.hwinj.schedule_state
        elif current_gps_time > self.hwinj.schedule_time:
            return "FAILURE_INJECT_IN_PAST"

        return False

class INJECT_JUMP_TRANSITION(injtools.HwinjGuardState):
    """ This state acts as a dummy edge for the _INJECT_STATE_ACTIVE states.
    The operator requests the INJECT_SUCCESS state so we need edges from
    WAIT_FOR_NEXT_INJECT->INJECT_SUCCESS. This acts as a placeholder for the
    connection between AWG_STREAM_OPEN_PREINJECT->INJECT_SUCCESS.

    If this state is ever reached, then its a failure.
    """

   # assign index for state
    index = 100

    # determines if state appears on guardian MEDM screen dropdown menu
    request = False

    def main(self):
        """ Execute method once.
        """

        return "FAILURE_MISSED_INJECT_JUMP_TRANSITION"

class _INJECT_STATE_ACTIVE(injtools.HwinjGuardState):
    """ The _INJECT_STATE_ACTIVE state is a subclass that injects the signal
    into the detector. The signal is injected using the
    awg.ArbitraryStream.close class function.

    The _INJECT_STATE_ACTIVE state will read the waveform file and upload a
    hardware injection event to GraceDB upon entry.
    """

    # determines if state appears on guardian MEDM screen dropdown menu
    request = False

    @check_exttrig_alert(hwinj_list, "ABORT_INJECT_FOR_EXTTRIG")
    def main(self):
        """ Execute method once.
        """

        # get hardware injection in the future that is soonest
        self.hwinj = injtools.check_imminent_injection(hwinj_list, imminent_seconds)
        if not self.hwinj: return "FAILURE_INJECT_IN_PAST"

        # close stream and perform injection
        # waits for injection to finish
        try:
            self.hwinj.stream.close()
        except:
            message = traceback.print_exc(file=sys.stdout)
            log(message)
            return "FAILURE_DURING_ACTIVE_INJECT"

        # check if stream is open when it should be closed
        if self.hwinj.stream.opened:
            self.hwinj.stream.abort()
            return "FAILURE_AWG_STREAM_NOT_CLOSED"

        return True

class INJECT_CBC_ACTIVE(_INJECT_STATE_ACTIVE):
    """ The INJECT_CBC_ACTIVE state will perform a CBC hardware injection.
    """

    # assign index for state
    index = 101

class INJECT_BURST_ACTIVE(_INJECT_STATE_ACTIVE):
    """ The INJECT_BURST_ACTIVE state will perform a burst hardware injection.
    """

    # assign index for state
    index = 102

class INJECT_STOCHASTIC_ACTIVE(_INJECT_STATE_ACTIVE):
    """ The INJECT_STOCHASTIC_ACTIVE state will perform a stochastic
    hardware injection.
    """

    # assign index for state
    index = 103

class INJECT_DETCHAR_ACTIVE(_INJECT_STATE_ACTIVE):
    """ The INJECT_DETCHAR_ACTIVE state will perform a detector
     characterization hardware injection.
    """

    # assign index for state
    index = 104

class INJECT_SUCCESS(injtools.HwinjGuardState):
    """ The INJECT_SUCCESS state is an intermediary state for an injection
    that was successfully performed. There is a jump transition to the
    WAIT_FOR_NEXT_INJECT state.
    """

    # assign index for state
    index = 200

    @kill_all_streams(hwinj_list)
    @gracedb_post_inj_message(hwinj_list, "Injection was successful.", imminent_seconds)
    def main(self):
        """ Execute method once.
        """

        # set legacy TINJ_OUTCOME value for successful injection
        ezca[outcome_channel_name] = 1

        # legacy of the old setup to set TINJ_END_TIME
        current_gps_time = gpstime.tconvert("now").gps()
        ezca[end_channel_name] = current_gps_time

        return "WAIT_FOR_NEXT_INJECT"

class INJECT_KILL(injtools.HwinjGuardState):
    """ Request INJECT_KILL to end a currently running injection.
    The INJECT_KILL state will close all streams for all injections
    loaded from the schedule.
    """

    # assign index for state
    index = 210

    # determines if state appears on guardian MEDM screen dropdown menu
    request = True

    # automatically assign edges from every other state
    goto = True

    @kill_all_streams(hwinj_list)
    @gracedb_post_inj_message(hwinj_list, "Injection was killed.", imminent_seconds)
    def main(self):
        """ Execute method once.
        """
        # set legacy TINJ_OUTCOME value for killed injection
        ezca[outcome_channel_name] = -11

        return True

    def run(self):
        """ Execute method in a loop.
        """
        return True

class ABORT_INJECT_FOR_EXTTRIG(injtools.HwinjGuardState):
    """  The ABORT_INJECT_FOR_EXTTRIG state is for a hardware injection that
    was aborted because of an external alert. It will make a jump transition
    to the EXTTRIG_ALERT_ACTIVE state.
    """

    # assign index for state
    index = 220

    # determines if state appears on guardian MEDM screen dropdown menu
    request = False

    @kill_all_streams(hwinj_list)
    @gracedb_post_inj_message(hwinj_list, "Injection was aborted for external trigger alert.", imminent_seconds)
    def main(self):
        """ Execute method once.
        """

        # set legacy TINJ_OUTCOME value for failed injection
        ezca[outcome_channel_name] = -4

        # legacy of the old setup to set TINJ_END_TIME
        current_gps_time = gpstime.tconvert("now").gps()
        ezca[end_channel_name] = current_gps_time

        return "EXTTRIG_ALERT_ACTIVE"

class _INJECT_FAILURE(injtools.HwinjGuardState):
    """ The INJECT_FAILURE state is for a hardware injection that
    was not successfully performed because of an unexpected error.

    The guardian node should remain in the _INJECT_FAILURE state
    unless the operator has requested another state.
    """

    # determines if state appears on guardian MEDM screen dropdown menu
    request = False

    @kill_all_streams(hwinj_list)
    @gracedb_post_inj_message(hwinj_list, "Injection failed.", imminent_seconds)
    def main(self):
        """ Execute method once.
        """

        # set legacy TINJ_OUTCOME value for failed injection
        ezca[outcome_channel_name] = -4

        # legacy of the old setup to set TINJ_END_TIME
        current_gps_time = gpstime.tconvert("now").gps()
        ezca[end_channel_name] = current_gps_time

        return True

    def run(self):
        """ Execute method in a loop.
        """

        # raise an error message for the operator to notice the MEDM
        # screen turn red
        error_message = "You are in the %s state and should check"%str(self) \
            + " the log for any errors. Reload the guardian node, once the" \
            + " issue is resolved."
        raise injtools.GuardStateError(error_message)

        return True

class FAILURE_CREATE_GRACEDB_EVENT(_INJECT_FAILURE):
    """ The FAILURE_CREATE_GRACEDB_EVENT state is for an unexpected error while
    uploading an event to GraceDB.
    """

    # assign index for state
    index = 230

class FAILURE_READ_WAVEFORM(_INJECT_FAILURE):
    """ The FAILURE_READ_WAVEFORM state is for an unexpected error while
    reading the waveform file.
    """

    # assign index for state
    index = 240

class FAILURE_INJECT_IN_PAST(_INJECT_FAILURE):
    """ The FAILURE_INJECT_IN_PAST state is a state used to log
    when a hardware injection was aborted because the scheduled
    injection has already past in time before the guardian node
    was able to transition to the _INJECT_STATE_ACTIVE to perform the
    injection.

    This is a safeguard. The schedule validation script should be
    run whenever updating the schedule file.
    """

    # assign index for state
    index = 250

class FAILURE_TO_FIND_GRACEDB_ID(_INJECT_FAILURE):
    """ The FAILURE_TO_FIND_GRACEDB_ID state indicates that the guardian state
    could not find the GraceDB ID of the hardware injection.

    For debugging purposes GraceDB IDs are stored as a class attribute
    HardwareInjection.gracedb_id.
    """

    # assign index for state
    index = 260

class FAILURE_ADDING_GRACEDB_MESSAGE(_INJECT_FAILURE):
    """ The FAILURE_ADDING_GRACEDB_MESSAGE state indicates that there was
    an error uploading a message to GraceDB.

    For debugging purposes check that you can ping GraceDB.
    """

    # assign index for state
    index = 261

class FAILURE_KILL_STREAM(_INJECT_FAILURE):
    """ The FAILURE_KILL_STREAM state indicates that there was a stream that
    may not have been aborted.
    """

    # assign index for state
    index = 270

    @gracedb_post_inj_message(hwinj_list, "There was a failure to kill the injection.", imminent_seconds)
    def main(self):
        """ Execute method once.
        """

        # set legacy TINJ_OUTCOME value for failed injection
        ezca[outcome_channel_name] = -4

        # legacy of the old setup to set TINJ_END_TIME
        current_gps_time = gpstime.tconvert("now").gps()
        ezca[end_channel_name] = current_gps_time

        return True

class FAILURE_AWG_STREAM_NOT_CLOSED(_INJECT_FAILURE):
    """ The FAILURE_AWG_STREAM_NOT_CLOSED state indicates that after performing
    the injection, that awg did not close the stream when it should have.
    """

    # assign index for state
    index = 280

class FAILURE_MISSED_INJECT_JUMP_TRANSITION(_INJECT_FAILURE):
    """ The FAILURE_MISSED_INJECT_JUMP_TRANSITION state indicates that
    guardian somehow did not go into a _INJECT_STATE_ACTIVE state.
    """

    # assign index for state
    index = 290

class FAILURE_DURING_ACTIVE_INJECT(_INJECT_FAILURE):
    """ The FAILURE_DURING_ACTIVE_INJECT state indicates that
    the call to awg.ArbitraryStream.close failed.
    """

    # assign index for state
    index = 300

class FAILURE_SCHEDULED_TWO_INJECT_TOO_CLOSE(_INJECT_FAILURE):
    """ The FAILURE_SCHEDULED_TWO_INJECT_TOO_CLOSE state indicates that
    the schedule file has two injections that are too close together.
    """

    # assign index for state
    index = 310

# define directed edges that connect guardian states
edges = (
    # edges that happen before a successful injection
    ("CHECK_SCHEDULE_TIMES", "CREATE_GRACEDB_EVENT"),
    ("CREATE_GRACEDB_EVENT", "CREATE_AWG_STREAM"),
    ("CREATE_AWG_STREAM", "READ_WAVEFORM"),
    ("READ_WAVEFORM", "AWG_STREAM_OPEN_PREINJECT"),
    # these are edges for starting the node
    ("INIT", "DISABLED"),
    ("DISABLED", "WAIT_FOR_NEXT_INJECT"),
    # these are edges with the INJECT_JUMP_TRANSITION state
    ("AWG_STREAM_OPEN_PREINJECT", "INJECT_JUMP_TRANSITION"),
    ("INJECT_JUMP_TRANSITION", "INJECT_SUCCESS"),
    # edges that happen after a successful injection
    ("INJECT_CBC_ACTIVE", "INJECT_SUCCESS"),
    ("INJECT_BURST_ACTIVE", "INJECT_SUCCESS"),
    ("INJECT_STOCHASTIC_ACTIVE", "INJECT_SUCCESS"),
    ("INJECT_DETCHAR_ACTIVE", "INJECT_SUCCESS"),
    # edge for external alerts
    ("ABORT_INJECT_FOR_EXTTRIG", "EXTTRIG_ALERT_ACTIVE"),
)

